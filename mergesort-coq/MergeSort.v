From Coq Require Import ssreflect ssrfun ssrbool Program Recdef List Lia.
Import ListNotations.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Fixpoint split {A: Type} (xys: list A): list A * list A:=
  match xys with
  | [] => ([], [])
  | [x] => ([x], [])
  | x::y::xys => let (xs, ys) := split xys in (x::xs, y::ys)
  end.

Program Fixpoint merge_by {A: Type} (leb: A -> A -> bool) (xs ys: list A) {measure (length xs + length ys)}: list A:=
  match (xs, ys) with
  | ([], rest) | (rest, []) => rest
  | (x::xs, y::ys) =>
      if leb x y
      then x::(merge_by leb xs (y::ys))
      else y::(merge_by leb (x::xs) ys)
  end.
  Next Obligation.
  simpl; lia.
  Defined.

Definition list_ind2 {A: Type} (P: list A -> Prop):
  P []
  -> (forall (x: A), P [x])
  -> (forall (x y: A) (xs: list A), P xs -> P (x::y::xs))
  -> forall xs: list A, P xs
:= fun H0 H1 Hrec => fix IH (xs: list A): P xs:=
  match xs with
  | [] => H0
  | [x] => H1 x
  | x::y::xs => Hrec x y xs (IH xs)
  end.

Lemma split_length_l: forall {A: Type} (xys xs ys: list A),
  (xs, ys) = split xys
  -> length xs <= length xys /\ length ys <= length xys.
Proof.
  move=> A. elim/(list_ind2 (A:=A)).
  - move=> xs ys. case=>Hxs Hys. rewrite Hxs Hys//.
  - move=> x xs ys. case=> Hxs Hys. rewrite Hxs Hys/=. lia.
  - move=> x y xs/=. elim (split xs). move=> xs1 ys1 Hrec xs0 ys. case=> Hxs1 Hys1. rewrite Hxs1 Hys1/=. elim (Hrec xs1 ys1)=>[P1 P2|]//. lia.
Qed.

Function merge_sort_by {A: Type} (le: A -> A -> bool) (xs: list A) {measure length xs}: list A:=
  match xs with
  | [] => []
  | [x] => [x]
  | y::z::xs =>
    let (ys, zs):= split (y::z::xs)
    in merge_by le (merge_sort_by le ys) (merge_sort_by le zs)
  end.
  Proof.
  - move=> A le xs y _ z xs0 _ Hxs0 ys zs/=. case E: (split xs0) => [ys0 zs0]. case. case zs => [|z1 zs1 _]//. case=>_ Hz/=. rewrite Hz in E. destruct (split_length_l (A:=A) (eq_sym E)). lia.
  - move=> A le xs y _ z xs0 _ Hxs0 ys zs/=. case E: (split xs0) => [ys0 zs0]. case. case ys => [|y1 ys1]//. case=>_ Hy _/=. rewrite Hy in E. destruct (split_length_l (A:=A) (eq_sym E)). lia.
  Defined.

From Coq Require Import Sorting.Sorted Sorting.Permutation.

Lemma merge_sorted: forall {A: Type} (leb: A -> A -> bool) (xs ys: list A), Sorted leb xs -> Sorted leb ys -> Sorted leb (merge_by leb xs ys).
Proof.
Admitted.


Theorem merge_sort_sorted: forall {A: Type} (leb: A -> A -> bool) (xs: list A), Sorted leb (merge_sort_by leb xs).
move=> A leb xs. elim xs using (merge_sort_by_ind)=> xs0//.
- move=> x Exs0. apply Sorted_cons => //.
- move=> y z xs1 Exs0 ys zs _. apply merge_sorted.
Qed.

Lemma split_permutation: forall {A: Type} (leb: A -> A -> bool) (xs ys zs: list A), (ys, zs) = split xs -> Permutation (ys  ++ zs) xs.
Proof.
Admitted.

Lemma merge_permutation: forall {A: Type} (leb: A -> A -> bool) (xs ys: list A), Permutation (xs ++ ys) (merge_by leb xs ys).
Proof.
Admitted.

Theorem merge_sort_permutation: forall {A: Type} (leb: A -> A -> bool) (xs: list A), Permutation xs (merge_sort_by leb xs).
Proof.
move=> A leb xs. elim xs using merge_sort_by_ind=> xs0 //.
move=> y z xs1 Exs0 ys zs Esplit IHys IHzs.
rewrite -merge_permutation -(split_permutation leb (eq_sym Esplit)).
apply Permutation_app => //.
Qed.


Import Arith.Compare_dec.

Compute (merge_sort_by leb [2; 4; 1; 2]).
