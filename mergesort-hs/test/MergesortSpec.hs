module MergesortSpec (spec) where

import Test.Hspec
import Test.QuickCheck

import Mergesort (mergesort)

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

sorted :: [Int] -> Bool
sorted [] = True
sorted [x] = True
sorted (x1:x2:xs) = x1 <= x2 && sorted (x2:xs)

permutation :: [Int] -> [Int] -> Bool
permutation xs ys = counter xs == counter ys
  where
    counter = foldl (\m x -> Map.insertWith (+) x 1 m) Map.empty

spec :: Spec
spec = do
  describe "permutations" $ do
    it "have the same length" $ property $
      \xs ys -> length xs /= length ys ==> not (permutation xs ys)
    it "have the same element counts" $ property $
      \x y -> x /= y ==> not (permutation [x, x, y] [y, y, x])
    it "are the same elements shuffled" $ property $
      \xs -> forAll (shuffle xs) $ \ys -> permutation xs ys
  describe "mergesort" $ do
    it "sorts in ascending order" $ property $
      \xs -> sorted (mergesort xs)
    it "perserves elements" $ property $
      \xs -> permutation xs (mergesort xs)
