def swap(xs, i, j):
    xs[i], xs[j] = xs[j], xs[i]


def merge(xs, lo, mid, hi):
    while lo <= mid and mid < hi:
        if xs[lo] <= xs[mid]:
            lo += 1
        else:
            for i in reversed(range(lo, mid)):
                swap(xs, i, i + 1)
            lo += 1
            mid += 1


def mergesort(xs, lo=0, hi=None):
    hi = hi or len(xs)
    if hi - lo > 1:
        mid = lo + (hi - lo) // 2
        mergesort(xs, lo, mid)
        mergesort(xs, mid, hi)
        merge(xs, lo, mid, hi)
