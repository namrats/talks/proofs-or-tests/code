import pytest
from hypothesis import assume, given
from hypothesis.strategies import lists, integers
from collections import Counter
from more_itertools import windowed

from mergesort import mergesort


def is_sorted(xs: list) -> bool:
    return len(xs) <= 1 or all(x <= y for x, y in windowed(xs, 2))


# @pytest.mark.prop
# @given(lists(integers()))
# def test_mergesort_sorted(xs: list):
#     mergesort(xs)
#     assert is_sorted(xs)


# def is_permutation(xs: list, ys: list) -> bool:
#     from itertools import permutations
#     return xs in permutations(ys)
#     # return Counter(xs) == Counter(ys)


# @pytest.mark.prop
# @given(lists(integers()))
# def test_mergesort_permutation(xs: list):
#     old_xs = list(xs)
#     mergesort(xs)
#     assert is_permutation(xs, old_xs)
