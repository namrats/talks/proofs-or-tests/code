import pytest

from mergesort import mergesort


@pytest.mark.case
@pytest.mark.parametrize("xs, expected", [
    ([], []),
    ([1], [1]),
    ([1, 2, 3], [1, 2, 3]),
    ([2, 1], [1, 2]),
    ([10, 9, 8, 7, 6, 5, 4, 3, 2, 1], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    ([1, 10, 8, 5, 6, 4, -1, 22], [-1, 1, 4, 5, 6, 8, 10, 22]),
    (["cup", "ask", "ball"], ["ask", "ball", "cup"])
])
def test_mergesort(xs, expected):
    mergesort(xs)
    assert xs == expected
